import { Injectable } from '@nestjs/common';
import { BehaviorSubject, Subject } from 'rxjs';
import { DisasterResponse } from './payload/disaster.response';

@Injectable()
export class AppService {
  private notificationSubscription: Subject<DisasterResponse> = new BehaviorSubject<DisasterResponse>(
    new DisasterResponse(),
  );

  public onNewNotification(callback: (message: DisasterResponse) => void) {
    return this.notificationSubscription.subscribe(callback);
  }

  public notify(message: DisasterResponse) {
    this.notificationSubscription.next(message);
  }
}
