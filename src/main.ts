import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { BROKER_OPTS, BROKER_TRANSPORT } from './broker.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice({
    transport: BROKER_TRANSPORT,
    options: BROKER_OPTS,
  });
  await app.startAllMicroservicesAsync();

  await app.listen(3000);
}
bootstrap();
