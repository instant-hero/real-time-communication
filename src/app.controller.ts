import { EventPattern, Payload } from '@nestjs/microservices';
import { Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { DisasterResponse } from './payload/disaster.response';

@Controller()
export class AppController {
  constructor(private appService: AppService) {}

  @EventPattern('disaster_happened')
  getHello(@Payload() data: DisasterResponse): void {
    this.appService.notify(data);
  }
}
