import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketGateway,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { AppService } from './app.service';

@WebSocketGateway({
  namespace: 'notifications',
  transports: 'websocket',
})
export class AppGateway implements OnGatewayConnection, OnGatewayDisconnect {
  private clients: { [id: string]: Socket } = {};

  constructor(private appService: AppService) {
    this.appService.onNewNotification((msg) => {
      console.log('new notification - start emitting');
      Object.values(this.clients).forEach((c) => {
        console.log('emtting to ' + c.id);
        c.emit('notification', msg);
      });
    });
  }

  handleConnection(client: Socket): void {
    console.log('connecting', client.id);
    this.clients[client.id] = client;
  }

  handleDisconnect(client: Socket): void {
    console.log('disconnecting', client.id);
    delete this.clients[client.id];
  }
}
